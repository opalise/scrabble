<?php

namespace Services\PlayerEditing;

use Models\Player;
use Services\Repository\PlayerSaver;

class PlayerAdder
{
    private $playerFactory;
    private $playerFieldsTransferrer;
    private $playerSaver;

    public function __construct(
        PlayerFactory $playerFactory,
        PlayerFieldsTransferrer $playerFieldsTransferrer,
        PlayerSaver $playerSaver
    ) {
        $this->playerFactory=$playerFactory;
        $this->playerFieldsTransferrer=$playerFieldsTransferrer;
        $this->playerSaver=$playerSaver;
    }

    public function addPlayer($inputVariables) {
        $player=$this->playerFactory->create();
        $this->playerFieldsTransferrer->transfer($inputVariables,$player);
        $this->playerSaver->savePlayer($player);
    }
}
