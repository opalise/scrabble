<?php

namespace Services\PlayerEditing;

use Models\Player;

class PlayerFactory
{
    public function create()
    {
        return new Player();
    }
}
