<?php

namespace Services\PlayerEditing;

class PlayerFieldsTransferrer
{
    public function transfer($inputVariables,$player)
    {
        $player->firstName=$inputVariables['firstName'];
        $player->lastName=$inputVariables['lastName'];
        $player->telephone=$inputVariables['telephone'];
        $player->emailAddress=$inputVariables['emailAddress'];
    }
} 
