<?php

namespace Services\PlayerEditing;

use Services\Repository\PlayerSaver;
use Services\Repository\PlayerByIdProvider;

class PlayerUpdater
{
    private $playerByIdProvider;
    private $playerFieldsTransferrer;
    private $playerSaver;

    public function __construct(
        PlayerByIdProvider $playerByIdProvider,
        PlayerFieldsTransferrer $playerFieldsTransferrer,
        PlayerSaver $playerSaver
    ) {
        $this->playerByIdProvider=$playerByIdProvider;
        $this->playerFieldsTransferrer=$playerFieldsTransferrer;
        $this->playerSaver=$playerSaver;
    }

    public function updatePlayer($playerId,$inputVariables) {
        $player=$this->playerByIdProvider->getPlayerById($playerId);
        $this->playerFieldsTransferrer->transfer($inputVariables,$player);
        $this->playerSaver->savePlayer($player);
    }
}
