<?php

namespace Services\Repository;

use \DB;

class HighestScoreDetailsProvider
{
    public function getHighestScoreDetailsByPlayerId($playerId)
    {
        $rows=DB::Select(
            DB::Raw(
                'select gameId, player1Id as playerId, player2Id as opponentPlayerId, player1Score as playerScore, player2Score as opponentScore, date_format(whenPlayed,\'%D %M %Y\') as whenPlayed, locationId from game where player1Id = :playerIdFirstInstance union select gameId, player2Id as playerId, player1Id as opponentPlayerId, player2Score as playerScore, player1Score as opponentPlayerScore, date_format(whenPlayed,\'%D %M %Y\') as whenPlayed, locationId from game where player2Id = :playerIdSecondInstance order by playerScore desc limit 1'
            ),
            array(
                'playerIdFirstInstance' => $playerId,
                'playerIdSecondInstance' => $playerId
            )
        );
        return $rows[0];
    }
}

