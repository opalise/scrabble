<?php

namespace Services\Repository;

use \DB;

class NumberOfWinsProvider
{
    public function getNumberOfWinsByPlayerId($playerId)
    {
        $rows=DB::Select(
            DB::Raw(
                'select count(gameId) as numberOfWins from (select gameId from game where player1Id = :playerIdFirstInstance and player1Score>player2Score union select gameId from game where player2Id = :playerIdSecondInstance and player2Score>player1Score) as playerWins'
            ),
            array(
                'playerIdFirstInstance' => $playerId,
                'playerIdSecondInstance' => $playerId
            )
        );
        return $rows[0]->numberOfWins;
    }
}

