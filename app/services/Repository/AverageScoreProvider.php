<?php

namespace Services\Repository;

use \DB;

class AverageScoreProvider
{
    public function getAverageScoreByPlayerId($playerId)
    {
        $rows=DB::Select(
            DB::Raw(
                'select round(avg(playerScore)) as averageScore from (select player1Score as playerScore from game where player1Id = :playerIdFirstInstance union select player2Score as playerScore from game where player2Id = :playerIdSecondInstance) as playerScores'
            ),
            array(
                'playerIdFirstInstance' => $playerId,
                'playerIdSecondInstance' => $playerId
            )
        );
        return $rows[0]->averageScore;
    }
}

