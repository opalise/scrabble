<?php

namespace Services\Repository;

use Models\Player;

class PlayersProvider
{
    public function getPlayers()
    {
        return Player::orderBy('firstName')->get();
    }
}

