<?php

namespace Services\Repository;

use \DB;

class NumberOfLossesProvider
{
    public function getNumberOfLossesByPlayerId($playerId)
    {
        $rows=DB::Select(
            DB::Raw(
                'select count(gameId) as numberOfLosses from (select gameId from game where player1Id = :playerIdFirstInstance and player1Score<player2Score union select gameId from game where player2Id = :playerIdSecondInstance and player2Score<player1Score) as playerScores'
            ),
            array(
                'playerIdFirstInstance' => $playerId,
                'playerIdSecondInstance' => $playerId
            )
        );
        return $rows[0]->numberOfLosses;
    }
}

