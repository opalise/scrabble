<?php

namespace Services\Repository;

use Models\Player;

class PlayerByIdProvider
{
    public function getPlayerById($playerId)
    {
        return Player::find($playerId);
    }
}
