<?php

namespace Services\Repository;

use \DB;

class NumberOfGamesPlayedProvider
{
    public function getNumberOfGamesPlayedByPlayerId($playerId)
    {
        $rows=DB::Select(
            DB::Raw(
                'select count(gameId) as numberOfGamesPlayed from (select gameId from game where player1Id = :playerIdFirstInstance union select gameId from game where player2Id = :playerIdSecondInstance) as gamesPlayed'
            ),
            array(
                'playerIdFirstInstance' => $playerId,
                'playerIdSecondInstance' => $playerId
            )
        );
        return $rows[0]->numberOfGamesPlayed;
    }
}

