<?php

namespace Services\Repository;

use Models\Player;

class PlayerSaver
{
    public function savePlayer(Player $player)
    {
        $player->save();
    }
}
