<?php

Route::pattern('playerId', '\d+');

Route::get(
    '/',
    'Controllers\ScrabbleController@showPlayerList'
);

Route::get(
    '/player/{playerId}/profile',
    'Controllers\ScrabbleController@showPlayerProfile'
);

Route::get(
    '/player/{playerId}/edit',
    'Controllers\ScrabbleController@editPlayer'
);

Route::post(
    '/player/{playerId}/edit-submit',
    'Controllers\ScrabbleController@editPlayerSubmit'
);

Route::get(
    '/add-player',
    'Controllers\ScrabbleController@addPlayer'
);

Route::post(
    '/add-player-submit',
    'Controllers\ScrabbleController@addPlayerSubmit'
);

