<?php

namespace Models;

class Player extends \Eloquent
{
    protected $table = 'player';
    protected $primaryKey = 'playerId';
    public $timestamps = false;

}
