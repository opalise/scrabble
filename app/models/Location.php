<?php

namespace Models;

class Location extends \Eloquent
{
    protected $table = 'location';
    protected $primaryKey = 'locationId';
    public $timestamps = false;

}
