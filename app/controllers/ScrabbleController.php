<?php

namespace Controllers;

use \App;
use \View;
use \Input;
use \Redirect;

use Models\Player;
use Models\Location;

class ScrabbleController extends BaseController {

    public function showPlayerList()
    {
        $playersProvider=App::make('Services\Repository\PlayersProvider');
        return View::make(
            'player-list',
            array(
                'players' => $playersProvider->getPlayers()
            )
        );        
    }

    public function showPlayerProfile($playerId)
    {
        $numberOfGamesPlayedProvider=App::make('Services\Repository\NumberOfGamesPlayedProvider');
        if ($numberOfGamesPlayedProvider->getNumberOfGamesPlayedByPlayerId($playerId)>0) {
            $averageScoreProvider=App::make('Services\Repository\AverageScoreProvider');
            $highestScoreDetailsProvider=App::make('Services\Repository\HighestScoreDetailsProvider');
            $numberOfLossesProvider=App::make('Services\Repository\NumberOfLossesProvider');
            $numberOfWinsProvider=App::make('Services\Repository\NumberOfWinsProvider');
            $highestScoreDetails=$highestScoreDetailsProvider->getHighestScoreDetailsByPlayerId($playerId);

            return View::make(
                'player-profile',
                array(
                    'player' => Player::find($playerId),
                    'averageScore' => $averageScoreProvider->getAverageScoreByPlayerId($playerId),
                    'numberOfWins' => $numberOfWinsProvider->getNumberOfWinsByPlayerId($playerId),
                    'numberOfLosses' => $numberOfLossesProvider->getNumberOfLossesByPlayerId($playerId),
                    'highestScore' => array(
                        'score' => $highestScoreDetails->playerScore,
                        'when' => $highestScoreDetails->whenPlayed,
                        'where' => Location::find($highestScoreDetails->locationId),
                        'againstPlayer' => Player::find($highestScoreDetails->opponentPlayerId)
                    )
                )
            );
        } else {
            return View::make(
                'player-profile-unavailable',
                array(
                    'player' => Player::find($playerId)
                )
            );
        }
    }

    public function addPlayer()
    {
        return View::make(
            'add-player'
        ); 
    }

    public function addPlayerSubmit()
    {
        $playerAdder=App::make('Services\PlayerEditing\PlayerAdder');
        $playerAdder->addPlayer(Input::all());
        return Redirect::action(__CLASS__.'@showPlayerList');
    }

    public function editPlayer($playerId)
    {
        return View::make(
            'edit-player',
            array(
                'player' => Player::find($playerId)
            )
        ); 
    }

    public function editPlayerSubmit($playerId)
    {
        $playerUpdater=App::make('Services\PlayerEditing\PlayerUpdater');
        $playerUpdater->updatePlayer($playerId,Input::all());
        return Redirect::action(__CLASS__.'@showPlayerList');
    }

}
