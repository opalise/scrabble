create table player (
    playerId int(11) auto_increment not null,
    firstName varchar(50) not null,
    lastName varchar(50) not null,
    telephone varchar(50) not null,
    emailAddress varchar(50) not null,
    primary key (playerId)
) engine=InnoDB charset=UTF8;

create table location (
    locationId int(11) auto_increment not null,
    name varchar(50) not null,
    primary key (locationId)
);

create table game (
    gameId int(11) auto_increment not null,
    player1Id int(11) not null,
    player1Score int(11) not null,
    player2Id int(11) not null,
    player2Score int(11) not null,
    locationId int(11) not null,
    whenPlayed date not null,
    primary key (gameId),
    foreign key (player1Id) references player(playerId),
    foreign key (player2Id) references player(playerId)
) engine=InnoDB charset=UTF8;

insert into location (name) values ('Jeremy\'s place');
insert into location (name) values ('Richard\'s place');
insert into location (name) values ('James\'s place');

insert into player (firstName,lastName,telephone,emailAddress) values ('Jayson','Leutwiler','0113 1234567','jayson@hotmail.com');
insert into player (firstName,lastName,telephone,emailAddress) values ('Jermaine','Grandison','0114 3483444','grandison@yahoo.com');
insert into player (firstName,lastName,telephone,emailAddress) values ('Mickey','Demetriou','01423 943812','mickeyd@outlook.com');
insert into player (firstName,lastName,telephone,emailAddress) values ('Ryan','Woods','01976 904768','woody@woody.com');
insert into player (firstName,lastName,telephone,emailAddress) values ('Mark','Ellis','0113 2326138','ellis@msn.com');
insert into player (firstName,lastName,telephone,emailAddress) values ('Connor','Goldson','0181 8421220','thegoldster@freemail.com');
insert into player (firstName,lastName,telephone,emailAddress) values ('Ashley','Vincent','01874 658765','avincent24@freeserve.co.uk');
insert into player (firstName,lastName,telephone,emailAddress) values ('James','Collins','0117 4006003','jamesc@outlook.com');
insert into player (firstName,lastName,telephone,emailAddress) values ('Scott','Vernon','0113 2201346','scottvernon@yahoo.com');

insert into game (player1Id,player1Score,player2Id,player2Score,locationId,whenPlayed) values (4,105,6,81,2,'2014-11-12');
insert into game (player1Id,player1Score,player2Id,player2Score,locationId,whenPlayed) values (1,176,4,173,3,'2013-12-28');
insert into game (player1Id,player1Score,player2Id,player2Score,locationId,whenPlayed) values (8,85,7,71,1,'2013-06-16');
insert into game (player1Id,player1Score,player2Id,player2Score,locationId,whenPlayed) values (2,172,7,120,2,'2014-01-19');
insert into game (player1Id,player1Score,player2Id,player2Score,locationId,whenPlayed) values (8,222,2,186,3,'2013-12-26');
insert into game (player1Id,player1Score,player2Id,player2Score,locationId,whenPlayed) values (7,90,2,50,1,'2013-06-24');
insert into game (player1Id,player1Score,player2Id,player2Score,locationId,whenPlayed) values (1,79,4,198,1,'2015-01-23');
insert into game (player1Id,player1Score,player2Id,player2Score,locationId,whenPlayed) values (3,185,8,142,3,'2013-08-23');
insert into game (player1Id,player1Score,player2Id,player2Score,locationId,whenPlayed) values (3,134,2,226,2,'2013-02-13');
insert into game (player1Id,player1Score,player2Id,player2Score,locationId,whenPlayed) values (4,85,3,100,1,'2013-01-29');
insert into game (player1Id,player1Score,player2Id,player2Score,locationId,whenPlayed) values (8,83,7,249,3,'2014-02-06');
insert into game (player1Id,player1Score,player2Id,player2Score,locationId,whenPlayed) values (8,238,9,166,3,'2013-03-08');
insert into game (player1Id,player1Score,player2Id,player2Score,locationId,whenPlayed) values (8,114,6,196,2,'2013-11-22');
insert into game (player1Id,player1Score,player2Id,player2Score,locationId,whenPlayed) values (8,144,7,114,3,'2013-05-10');
insert into game (player1Id,player1Score,player2Id,player2Score,locationId,whenPlayed) values (6,72,9,179,3,'2013-04-07');
insert into game (player1Id,player1Score,player2Id,player2Score,locationId,whenPlayed) values (4,230,1,70,2,'2013-09-08');
insert into game (player1Id,player1Score,player2Id,player2Score,locationId,whenPlayed) values (9,177,5,155,2,'2013-11-29');
insert into game (player1Id,player1Score,player2Id,player2Score,locationId,whenPlayed) values (4,91,2,192,3,'2013-12-28');
insert into game (player1Id,player1Score,player2Id,player2Score,locationId,whenPlayed) values (3,110,7,205,1,'2014-12-31');
insert into game (player1Id,player1Score,player2Id,player2Score,locationId,whenPlayed) values (6,241,7,199,2,'2013-05-10');
insert into game (player1Id,player1Score,player2Id,player2Score,locationId,whenPlayed) values (6,238,7,160,3,'2014-01-30');
insert into game (player1Id,player1Score,player2Id,player2Score,locationId,whenPlayed) values (3,244,8,226,1,'2014-04-30');
insert into game (player1Id,player1Score,player2Id,player2Score,locationId,whenPlayed) values (5,163,7,183,2,'2014-02-11');
insert into game (player1Id,player1Score,player2Id,player2Score,locationId,whenPlayed) values (2,100,7,152,2,'2014-05-28');
insert into game (player1Id,player1Score,player2Id,player2Score,locationId,whenPlayed) values (5,63,2,150,3,'2014-03-14');
insert into game (player1Id,player1Score,player2Id,player2Score,locationId,whenPlayed) values (4,86,5,109,1,'2015-01-05');
insert into game (player1Id,player1Score,player2Id,player2Score,locationId,whenPlayed) values (8,216,3,202,1,'2014-10-14');
insert into game (player1Id,player1Score,player2Id,player2Score,locationId,whenPlayed) values (2,221,6,190,1,'2014-07-14');
insert into game (player1Id,player1Score,player2Id,player2Score,locationId,whenPlayed) values (2,249,5,136,1,'2014-02-11');
insert into game (player1Id,player1Score,player2Id,player2Score,locationId,whenPlayed) values (7,171,6,217,1,'2014-02-18');
insert into game (player1Id,player1Score,player2Id,player2Score,locationId,whenPlayed) values (3,248,4,141,3,'2014-12-20');
insert into game (player1Id,player1Score,player2Id,player2Score,locationId,whenPlayed) values (5,112,4,108,2,'2014-02-28');
insert into game (player1Id,player1Score,player2Id,player2Score,locationId,whenPlayed) values (4,112,2,75,2,'2013-11-28');
insert into game (player1Id,player1Score,player2Id,player2Score,locationId,whenPlayed) values (3,165,1,197,1,'2014-12-19');
insert into game (player1Id,player1Score,player2Id,player2Score,locationId,whenPlayed) values (5,182,6,116,3,'2014-10-23');
insert into game (player1Id,player1Score,player2Id,player2Score,locationId,whenPlayed) values (6,74,2,60,3,'2014-09-22');
insert into game (player1Id,player1Score,player2Id,player2Score,locationId,whenPlayed) values (5,200,8,68,1,'2014-09-23');
insert into game (player1Id,player1Score,player2Id,player2Score,locationId,whenPlayed) values (3,110,6,214,1,'2013-11-12');
insert into game (player1Id,player1Score,player2Id,player2Score,locationId,whenPlayed) values (8,238,4,217,3,'2013-11-19');
insert into game (player1Id,player1Score,player2Id,player2Score,locationId,whenPlayed) values (5,117,2,176,3,'2014-01-13');
insert into game (player1Id,player1Score,player2Id,player2Score,locationId,whenPlayed) values (7,139,8,235,2,'2013-04-13');
insert into game (player1Id,player1Score,player2Id,player2Score,locationId,whenPlayed) values (5,179,2,162,2,'2013-06-09');
insert into game (player1Id,player1Score,player2Id,player2Score,locationId,whenPlayed) values (8,74,2,185,2,'2013-08-16');
insert into game (player1Id,player1Score,player2Id,player2Score,locationId,whenPlayed) values (5,63,2,144,3,'2013-04-06');
insert into game (player1Id,player1Score,player2Id,player2Score,locationId,whenPlayed) values (1,65,5,131,1,'2013-05-18');
insert into game (player1Id,player1Score,player2Id,player2Score,locationId,whenPlayed) values (9,185,8,124,3,'2014-12-30');
insert into game (player1Id,player1Score,player2Id,player2Score,locationId,whenPlayed) values (3,207,9,207,3,'2013-10-18');
insert into game (player1Id,player1Score,player2Id,player2Score,locationId,whenPlayed) values (9,113,8,105,2,'2013-05-18');
insert into game (player1Id,player1Score,player2Id,player2Score,locationId,whenPlayed) values (4,114,6,124,2,'2014-04-18');
insert into game (player1Id,player1Score,player2Id,player2Score,locationId,whenPlayed) values (8,210,6,233,2,'2013-08-17');
insert into game (player1Id,player1Score,player2Id,player2Score,locationId,whenPlayed) values (7,69,2,151,1,'2014-05-05');
insert into game (player1Id,player1Score,player2Id,player2Score,locationId,whenPlayed) values (7,107,5,153,1,'2015-01-14');
insert into game (player1Id,player1Score,player2Id,player2Score,locationId,whenPlayed) values (2,144,8,140,2,'2014-06-10');
insert into game (player1Id,player1Score,player2Id,player2Score,locationId,whenPlayed) values (8,178,1,94,2,'2015-01-06');
insert into game (player1Id,player1Score,player2Id,player2Score,locationId,whenPlayed) values (1,216,3,242,3,'2013-12-21');
insert into game (player1Id,player1Score,player2Id,player2Score,locationId,whenPlayed) values (7,180,9,92,1,'2015-01-15');
insert into game (player1Id,player1Score,player2Id,player2Score,locationId,whenPlayed) values (9,110,6,141,3,'2014-06-08');
insert into game (player1Id,player1Score,player2Id,player2Score,locationId,whenPlayed) values (6,89,1,65,2,'2013-10-08');
insert into game (player1Id,player1Score,player2Id,player2Score,locationId,whenPlayed) values (4,203,3,56,2,'2014-03-24');
insert into game (player1Id,player1Score,player2Id,player2Score,locationId,whenPlayed) values (1,195,6,229,2,'2014-01-20');
insert into game (player1Id,player1Score,player2Id,player2Score,locationId,whenPlayed) values (5,142,3,69,2,'2013-11-11');
insert into game (player1Id,player1Score,player2Id,player2Score,locationId,whenPlayed) values (2,93,4,133,3,'2014-12-12');
insert into game (player1Id,player1Score,player2Id,player2Score,locationId,whenPlayed) values (7,66,5,237,2,'2013-12-22');
insert into game (player1Id,player1Score,player2Id,player2Score,locationId,whenPlayed) values (6,225,9,120,3,'2014-03-30');
insert into game (player1Id,player1Score,player2Id,player2Score,locationId,whenPlayed) values (7,51,1,150,3,'2013-12-14');
insert into game (player1Id,player1Score,player2Id,player2Score,locationId,whenPlayed) values (1,214,4,144,2,'2014-07-01');
insert into game (player1Id,player1Score,player2Id,player2Score,locationId,whenPlayed) values (1,190,6,108,1,'2013-12-13');
insert into game (player1Id,player1Score,player2Id,player2Score,locationId,whenPlayed) values (4,57,7,138,3,'2013-11-27');
insert into game (player1Id,player1Score,player2Id,player2Score,locationId,whenPlayed) values (9,156,3,223,2,'2014-01-23');
insert into game (player1Id,player1Score,player2Id,player2Score,locationId,whenPlayed) values (3,160,4,108,3,'2014-02-08');
insert into game (player1Id,player1Score,player2Id,player2Score,locationId,whenPlayed) values (8,223,9,185,2,'2014-05-25');
insert into game (player1Id,player1Score,player2Id,player2Score,locationId,whenPlayed) values (9,236,5,182,3,'2013-03-18');
insert into game (player1Id,player1Score,player2Id,player2Score,locationId,whenPlayed) values (3,185,1,102,2,'2014-05-18');
insert into game (player1Id,player1Score,player2Id,player2Score,locationId,whenPlayed) values (8,173,5,125,2,'2014-08-16');
insert into game (player1Id,player1Score,player2Id,player2Score,locationId,whenPlayed) values (8,173,7,136,3,'2014-01-25');
insert into game (player1Id,player1Score,player2Id,player2Score,locationId,whenPlayed) values (9,131,8,207,2,'2013-07-10');
insert into game (player1Id,player1Score,player2Id,player2Score,locationId,whenPlayed) values (2,95,5,60,1,'2013-02-24');
insert into game (player1Id,player1Score,player2Id,player2Score,locationId,whenPlayed) values (9,61,3,182,2,'2013-11-21');
insert into game (player1Id,player1Score,player2Id,player2Score,locationId,whenPlayed) values (1,74,4,174,3,'2014-01-20');
insert into game (player1Id,player1Score,player2Id,player2Score,locationId,whenPlayed) values (8,86,7,143,1,'2014-10-14');
insert into game (player1Id,player1Score,player2Id,player2Score,locationId,whenPlayed) values (9,242,1,125,3,'2014-03-11');
insert into game (player1Id,player1Score,player2Id,player2Score,locationId,whenPlayed) values (2,228,9,125,1,'2013-01-31');
insert into game (player1Id,player1Score,player2Id,player2Score,locationId,whenPlayed) values (4,116,9,129,2,'2013-03-27');
insert into game (player1Id,player1Score,player2Id,player2Score,locationId,whenPlayed) values (9,122,6,72,1,'2014-09-25');
insert into game (player1Id,player1Score,player2Id,player2Score,locationId,whenPlayed) values (6,223,2,212,2,'2013-02-03');
insert into game (player1Id,player1Score,player2Id,player2Score,locationId,whenPlayed) values (9,57,5,233,3,'2013-05-09');
insert into game (player1Id,player1Score,player2Id,player2Score,locationId,whenPlayed) values (4,213,1,99,2,'2013-06-15');
insert into game (player1Id,player1Score,player2Id,player2Score,locationId,whenPlayed) values (3,209,8,164,1,'2014-03-14');
insert into game (player1Id,player1Score,player2Id,player2Score,locationId,whenPlayed) values (5,67,2,220,1,'2014-05-10');
insert into game (player1Id,player1Score,player2Id,player2Score,locationId,whenPlayed) values (1,146,9,228,3,'2014-12-11');
insert into game (player1Id,player1Score,player2Id,player2Score,locationId,whenPlayed) values (8,174,7,233,2,'2013-12-27');
insert into game (player1Id,player1Score,player2Id,player2Score,locationId,whenPlayed) values (7,174,9,168,1,'2014-12-08');
insert into game (player1Id,player1Score,player2Id,player2Score,locationId,whenPlayed) values (4,89,8,250,1,'2013-10-22');
insert into game (player1Id,player1Score,player2Id,player2Score,locationId,whenPlayed) values (5,195,3,118,2,'2014-11-27');
insert into game (player1Id,player1Score,player2Id,player2Score,locationId,whenPlayed) values (4,99,6,79,2,'2014-10-28');
insert into game (player1Id,player1Score,player2Id,player2Score,locationId,whenPlayed) values (8,58,3,132,3,'2013-06-10');
insert into game (player1Id,player1Score,player2Id,player2Score,locationId,whenPlayed) values (4,148,5,212,3,'2014-05-18');
insert into game (player1Id,player1Score,player2Id,player2Score,locationId,whenPlayed) values (1,129,9,178,2,'2013-11-04');
insert into game (player1Id,player1Score,player2Id,player2Score,locationId,whenPlayed) values (4,75,8,140,1,'2013-12-19');
insert into game (player1Id,player1Score,player2Id,player2Score,locationId,whenPlayed) values (1,191,4,181,2,'2014-01-10');
insert into game (player1Id,player1Score,player2Id,player2Score,locationId,whenPlayed) values (9,237,6,191,2,'2014-07-04');
insert into game (player1Id,player1Score,player2Id,player2Score,locationId,whenPlayed) values (2,215,8,172,2,'2013-07-02');
insert into game (player1Id,player1Score,player2Id,player2Score,locationId,whenPlayed) values (9,201,6,121,1,'2014-08-30');
insert into game (player1Id,player1Score,player2Id,player2Score,locationId,whenPlayed) values (9,50,6,70,1,'2014-10-04');
insert into game (player1Id,player1Score,player2Id,player2Score,locationId,whenPlayed) values (6,163,1,122,3,'2014-11-20');
insert into game (player1Id,player1Score,player2Id,player2Score,locationId,whenPlayed) values (8,184,6,214,2,'2014-12-15');
insert into game (player1Id,player1Score,player2Id,player2Score,locationId,whenPlayed) values (1,222,5,236,1,'2014-02-21');
insert into game (player1Id,player1Score,player2Id,player2Score,locationId,whenPlayed) values (7,66,1,144,2,'2014-05-31');
insert into game (player1Id,player1Score,player2Id,player2Score,locationId,whenPlayed) values (7,240,4,186,2,'2015-01-17');
insert into game (player1Id,player1Score,player2Id,player2Score,locationId,whenPlayed) values (8,69,2,131,2,'2013-05-29');
insert into game (player1Id,player1Score,player2Id,player2Score,locationId,whenPlayed) values (5,145,4,83,1,'2013-06-08');
insert into game (player1Id,player1Score,player2Id,player2Score,locationId,whenPlayed) values (3,95,1,123,2,'2013-03-10');
insert into game (player1Id,player1Score,player2Id,player2Score,locationId,whenPlayed) values (4,133,6,211,3,'2014-11-22');
insert into game (player1Id,player1Score,player2Id,player2Score,locationId,whenPlayed) values (2,205,9,180,3,'2013-11-07');
insert into game (player1Id,player1Score,player2Id,player2Score,locationId,whenPlayed) values (7,53,1,103,3,'2014-01-17');
insert into game (player1Id,player1Score,player2Id,player2Score,locationId,whenPlayed) values (6,187,4,218,1,'2013-04-03');
insert into game (player1Id,player1Score,player2Id,player2Score,locationId,whenPlayed) values (2,50,5,123,3,'2013-12-18');
insert into game (player1Id,player1Score,player2Id,player2Score,locationId,whenPlayed) values (3,196,2,57,1,'2013-06-09');
insert into game (player1Id,player1Score,player2Id,player2Score,locationId,whenPlayed) values (2,169,1,222,1,'2014-08-31');
insert into game (player1Id,player1Score,player2Id,player2Score,locationId,whenPlayed) values (6,94,1,231,3,'2013-08-07');
insert into game (player1Id,player1Score,player2Id,player2Score,locationId,whenPlayed) values (5,134,3,127,2,'2014-05-29');
insert into game (player1Id,player1Score,player2Id,player2Score,locationId,whenPlayed) values (5,117,8,228,3,'2013-04-18');
insert into game (player1Id,player1Score,player2Id,player2Score,locationId,whenPlayed) values (2,66,9,236,3,'2014-08-04');
insert into game (player1Id,player1Score,player2Id,player2Score,locationId,whenPlayed) values (7,114,2,118,1,'2014-06-01');
insert into game (player1Id,player1Score,player2Id,player2Score,locationId,whenPlayed) values (5,125,7,204,2,'2014-07-04');
insert into game (player1Id,player1Score,player2Id,player2Score,locationId,whenPlayed) values (5,158,2,237,2,'2013-02-26');
insert into game (player1Id,player1Score,player2Id,player2Score,locationId,whenPlayed) values (3,218,1,170,3,'2014-01-01');
insert into game (player1Id,player1Score,player2Id,player2Score,locationId,whenPlayed) values (5,144,2,165,1,'2014-03-17');
insert into game (player1Id,player1Score,player2Id,player2Score,locationId,whenPlayed) values (8,77,6,76,2,'2014-02-19');
insert into game (player1Id,player1Score,player2Id,player2Score,locationId,whenPlayed) values (7,219,3,140,3,'2014-10-26');
insert into game (player1Id,player1Score,player2Id,player2Score,locationId,whenPlayed) values (9,184,1,228,2,'2013-10-26');
insert into game (player1Id,player1Score,player2Id,player2Score,locationId,whenPlayed) values (2,144,5,201,2,'2015-01-25');
insert into game (player1Id,player1Score,player2Id,player2Score,locationId,whenPlayed) values (3,145,5,214,2,'2013-04-06');
insert into game (player1Id,player1Score,player2Id,player2Score,locationId,whenPlayed) values (6,154,4,205,2,'2014-09-30');
insert into game (player1Id,player1Score,player2Id,player2Score,locationId,whenPlayed) values (3,146,2,67,2,'2014-05-04');
insert into game (player1Id,player1Score,player2Id,player2Score,locationId,whenPlayed) values (2,124,6,227,2,'2013-04-29');
insert into game (player1Id,player1Score,player2Id,player2Score,locationId,whenPlayed) values (5,130,6,247,2,'2013-05-25');
insert into game (player1Id,player1Score,player2Id,player2Score,locationId,whenPlayed) values (9,128,6,142,2,'2013-02-09');
insert into game (player1Id,player1Score,player2Id,player2Score,locationId,whenPlayed) values (4,123,1,228,3,'2013-04-24');
insert into game (player1Id,player1Score,player2Id,player2Score,locationId,whenPlayed) values (1,88,5,194,3,'2014-04-05');
insert into game (player1Id,player1Score,player2Id,player2Score,locationId,whenPlayed) values (3,80,8,212,2,'2013-12-21');
insert into game (player1Id,player1Score,player2Id,player2Score,locationId,whenPlayed) values (8,206,4,143,3,'2014-08-06');
insert into game (player1Id,player1Score,player2Id,player2Score,locationId,whenPlayed) values (9,172,7,50,1,'2014-02-01');
insert into game (player1Id,player1Score,player2Id,player2Score,locationId,whenPlayed) values (8,160,1,245,1,'2014-11-30');
insert into game (player1Id,player1Score,player2Id,player2Score,locationId,whenPlayed) values (2,247,8,165,1,'2014-07-25');
insert into game (player1Id,player1Score,player2Id,player2Score,locationId,whenPlayed) values (8,131,9,185,2,'2013-02-26');
insert into game (player1Id,player1Score,player2Id,player2Score,locationId,whenPlayed) values (5,120,2,99,3,'2014-10-20');
insert into game (player1Id,player1Score,player2Id,player2Score,locationId,whenPlayed) values (5,232,6,69,2,'2014-12-29');
insert into game (player1Id,player1Score,player2Id,player2Score,locationId,whenPlayed) values (6,62,5,77,2,'2014-10-06');
insert into game (player1Id,player1Score,player2Id,player2Score,locationId,whenPlayed) values (2,245,6,90,1,'2014-11-13');
insert into game (player1Id,player1Score,player2Id,player2Score,locationId,whenPlayed) values (5,57,1,223,3,'2014-04-01');
insert into game (player1Id,player1Score,player2Id,player2Score,locationId,whenPlayed) values (8,174,2,85,2,'2014-06-30');
insert into game (player1Id,player1Score,player2Id,player2Score,locationId,whenPlayed) values (3,226,9,94,1,'2014-02-20');
insert into game (player1Id,player1Score,player2Id,player2Score,locationId,whenPlayed) values (3,236,6,114,3,'2014-05-14');
insert into game (player1Id,player1Score,player2Id,player2Score,locationId,whenPlayed) values (5,240,9,140,1,'2014-11-07');
insert into game (player1Id,player1Score,player2Id,player2Score,locationId,whenPlayed) values (6,169,2,145,3,'2015-01-21');
insert into game (player1Id,player1Score,player2Id,player2Score,locationId,whenPlayed) values (3,176,1,145,2,'2013-03-27');
insert into game (player1Id,player1Score,player2Id,player2Score,locationId,whenPlayed) values (8,210,4,52,2,'2014-07-17');
insert into game (player1Id,player1Score,player2Id,player2Score,locationId,whenPlayed) values (3,89,9,170,3,'2013-12-23');
insert into game (player1Id,player1Score,player2Id,player2Score,locationId,whenPlayed) values (1,149,6,156,3,'2013-11-09');
insert into game (player1Id,player1Score,player2Id,player2Score,locationId,whenPlayed) values (1,201,4,187,3,'2013-11-10');
insert into game (player1Id,player1Score,player2Id,player2Score,locationId,whenPlayed) values (7,178,2,114,2,'2014-11-25');
insert into game (player1Id,player1Score,player2Id,player2Score,locationId,whenPlayed) values (3,140,4,58,2,'2013-06-12');
insert into game (player1Id,player1Score,player2Id,player2Score,locationId,whenPlayed) values (3,206,7,150,1,'2014-02-20');
insert into game (player1Id,player1Score,player2Id,player2Score,locationId,whenPlayed) values (1,72,4,160,3,'2013-04-07');
insert into game (player1Id,player1Score,player2Id,player2Score,locationId,whenPlayed) values (2,111,1,233,3,'2014-09-17');
insert into game (player1Id,player1Score,player2Id,player2Score,locationId,whenPlayed) values (5,121,4,81,3,'2015-01-26');
insert into game (player1Id,player1Score,player2Id,player2Score,locationId,whenPlayed) values (3,138,9,190,3,'2013-04-17');
insert into game (player1Id,player1Score,player2Id,player2Score,locationId,whenPlayed) values (5,174,3,107,3,'2013-03-14');
insert into game (player1Id,player1Score,player2Id,player2Score,locationId,whenPlayed) values (7,118,8,224,2,'2014-07-16');
insert into game (player1Id,player1Score,player2Id,player2Score,locationId,whenPlayed) values (7,107,6,66,2,'2013-02-11');
insert into game (player1Id,player1Score,player2Id,player2Score,locationId,whenPlayed) values (3,124,9,174,1,'2014-12-01');
insert into game (player1Id,player1Score,player2Id,player2Score,locationId,whenPlayed) values (6,56,4,63,1,'2015-01-12');
insert into game (player1Id,player1Score,player2Id,player2Score,locationId,whenPlayed) values (9,114,6,165,3,'2014-11-01');
insert into game (player1Id,player1Score,player2Id,player2Score,locationId,whenPlayed) values (5,245,6,221,2,'2014-04-30');
insert into game (player1Id,player1Score,player2Id,player2Score,locationId,whenPlayed) values (2,237,3,131,2,'2014-04-06');
insert into game (player1Id,player1Score,player2Id,player2Score,locationId,whenPlayed) values (4,130,6,202,1,'2013-12-19');
insert into game (player1Id,player1Score,player2Id,player2Score,locationId,whenPlayed) values (8,242,9,225,3,'2014-12-21');
insert into game (player1Id,player1Score,player2Id,player2Score,locationId,whenPlayed) values (9,93,6,148,2,'2014-05-29');
insert into game (player1Id,player1Score,player2Id,player2Score,locationId,whenPlayed) values (1,112,2,223,3,'2013-09-17');
insert into game (player1Id,player1Score,player2Id,player2Score,locationId,whenPlayed) values (9,174,1,128,2,'2015-01-11');
insert into game (player1Id,player1Score,player2Id,player2Score,locationId,whenPlayed) values (8,135,1,158,2,'2013-02-11');
insert into game (player1Id,player1Score,player2Id,player2Score,locationId,whenPlayed) values (4,238,3,100,1,'2013-02-05');
insert into game (player1Id,player1Score,player2Id,player2Score,locationId,whenPlayed) values (2,181,1,122,2,'2014-08-26');
insert into game (player1Id,player1Score,player2Id,player2Score,locationId,whenPlayed) values (7,136,6,50,2,'2014-09-28');
insert into game (player1Id,player1Score,player2Id,player2Score,locationId,whenPlayed) values (7,91,4,110,3,'2013-10-26');
insert into game (player1Id,player1Score,player2Id,player2Score,locationId,whenPlayed) values (3,190,6,201,1,'2015-01-05');
insert into game (player1Id,player1Score,player2Id,player2Score,locationId,whenPlayed) values (7,101,5,184,3,'2014-03-13');
insert into game (player1Id,player1Score,player2Id,player2Score,locationId,whenPlayed) values (6,158,9,112,1,'2014-10-27');
insert into game (player1Id,player1Score,player2Id,player2Score,locationId,whenPlayed) values (5,195,9,241,3,'2014-09-16');
insert into game (player1Id,player1Score,player2Id,player2Score,locationId,whenPlayed) values (2,176,7,115,3,'2014-02-23');
insert into game (player1Id,player1Score,player2Id,player2Score,locationId,whenPlayed) values (9,66,3,181,1,'2014-08-06');
insert into game (player1Id,player1Score,player2Id,player2Score,locationId,whenPlayed) values (7,206,8,239,2,'2013-12-09');
insert into game (player1Id,player1Score,player2Id,player2Score,locationId,whenPlayed) values (4,151,2,235,2,'2013-06-25');
insert into game (player1Id,player1Score,player2Id,player2Score,locationId,whenPlayed) values (1,207,9,206,3,'2013-07-11');
insert into game (player1Id,player1Score,player2Id,player2Score,locationId,whenPlayed) values (9,141,1,168,2,'2014-03-19');
insert into game (player1Id,player1Score,player2Id,player2Score,locationId,whenPlayed) values (1,198,3,77,3,'2013-06-26');
insert into game (player1Id,player1Score,player2Id,player2Score,locationId,whenPlayed) values (4,168,6,81,2,'2014-12-21');
insert into game (player1Id,player1Score,player2Id,player2Score,locationId,whenPlayed) values (7,85,9,95,3,'2013-11-08');
insert into game (player1Id,player1Score,player2Id,player2Score,locationId,whenPlayed) values (1,164,9,212,3,'2014-02-11');

